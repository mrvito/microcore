# README #

# Microcore

A very small PHP framework

### What is this repository for? ###

* For those who want to create a quick project using php but don't want to write all the boilerplate from scratch
* This repository is only a project. Framework repository is [here](https://bitbucket.org/mrvito/microcore-framework.git)

### How do I get set up? ###

* Just create a project using composer: `composer create-project mrvito/microcore your_project_name`
* Edit the config.php file in the root of the project to set up your configuration options

### Who do I talk to? ###

* Repo owner or admin